const vscode = require('vscode');
const CodeGenCLI = require('code-gen/src/cli');

const extension = {

    actions: {},

    hasActions: false,

    init() {
        process.chdir(vscode.workspace.rootPath);
        for (let command of CodeGenCLI.commands) {
            if (typeof command.vsCodeCallback === 'function') {
                this.hasActions = true;
                this.actions[command.name()] = command;
            }
        }
    },

    prosesOptions(options, i, args, cb) {
        if(options.length === i) {
            cb(args);
            return;
        }

        let option = options[i];
        let next = (value) => {
            args[option.name] = value;
            this.prosesOptions(options, (i + 1), args, cb);
        };

        switch(option.type) {
            case 'input':
                vscode.window
                    .showInputBox(option.options || {})
                    .then(next);
                break;
            case 'dropdown':
                vscode.window
                    .showQuickPick(option.items, option.options || {})
                    .then(next);
                break;
        }
    },

    generate(item) {
        let command = extension.actions[item];
        let callback = () => {};
        let args = {
            workspaceRoot: vscode.workspace.rootPath
        };

        if (typeof command.vsCodeCallback === 'function') {
            callback = (args) => {
                Object.assign(command, args);
                let message = command.vsCodeCallback.apply(command, args);
                switch(message.type) {
                    case 'info':
                        vscode.window.showInformationMessage(
                            message.message,
                            message.options || {}
                        );
                        break;
                    case 'error':
                        vscode.window.showErrorMessage(
                            message.message,
                            message.options || {}
                        );
                        break;
                }
            }
        }

        if (typeof command.vsCodeOptions === 'object') {
            extension.prosesOptions(
                command.vsCodeOptions,
                0,
                args,
                callback
            );
        } else {
            callback(args);
        }
    }

};

exports.activate = (context) => {
    extension.init();

    let disposable = vscode.commands.registerCommand('code-gen.generate', function () {
        if (extension.hasActions === false) {
            return;
        }

        vscode.window
            .showQuickPick(Object.keys(extension.actions))
            .then(extension.generate);
    });

    context.subscriptions.push(disposable);
}

exports.deactivate = () => {
    console.log('CodeGen Deactivated');
};